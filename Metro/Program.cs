﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Metro
{
    class Program
    {
        static void Main(string[] args)
        {
            // 100 5 6
            // 10 1 9
            StreamReader reader = new StreamReader("input.txt");
            int N = int.Parse(reader.ReadLine());
            int a = int.Parse(reader.ReadLine());
            int b = int.Parse(reader.ReadLine());
            reader.Close();

            int R = a - b;

            if ((R == 1) || (R == -1))
            {
                R = 0;
            }

            else if (((R < -1) || (R > 1)) && (N == a + b))
            {
                R = N - b;
            }
            else if ((R > 1) && (a < b))
            {
                R = (b - a) - 1;
            }
            else if ((R > 1) && (a > b))
            {
                R = (a - b) - 1;
            }
            else if ((R < -1) && (a < b))
            {
                R = (b - a)-1 ;
            }
            
            StreamWriter writer = new StreamWriter("output.txt");
            writer.WriteLine($"Резуальтат: {R}");
            writer.Close();
        }
    }
}
